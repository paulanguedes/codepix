package model

type Transaction struct {
	Base 								'valid:"required"'
	AccountFrom 				*Account	'valid:"-"'
	Amount 							float64		'json:"amount" valid:"notnull"'
	PixKeyTo 						*PixKey		'valid:"-"'
	Status 							string		'json:"status" valid:"notnull"'
	Description					string		'json:"description" valid:"notnull"'
	CancelDescription		string		'json:"cancel_description" valid:"notnull"'

}